import sys
import os
import pandas as pd
from numpy.fft import fft, ifft
import numpy as np
import csv

# Dateipfad Knie, Dateipfad Kurbel, ExportFile.csv, Klasse (too_low, right, too_high)
if len(sys.argv) != 5:
    raise ValueError('1: Knie Acceleration 2: Kurbel Euler Angles 3:file_name 4: classification(too low, too high, right)')

knie = sys.argv[1]
kurbel = sys.argv[2]

df_knie = pd.read_csv(knie, usecols=[0,2,3,4,5])
df_kurbel = pd.read_csv(kurbel, usecols=[0,2,5])

df_knie['x-axis (m/s^2)'] = df_knie['x-axis (g)'].astype(float) * 9.80665
df_knie['y-axis (m/s^2)'] = df_knie['y-axis (g)'].astype(float) * 9.80665
df_knie['z-axis (m/s^2)'] = df_knie['z-axis (g)'].astype(float) * 9.80665

for ind in df_knie.index:
    if ind != 0 and df_knie['epoc (ms)'][ind] < df_knie['epoc (ms)'][ind-1]:
        print(f"Knie: Fehler in der Matrix. Zeitfehler der Epochen")
        print(f"{df_knie['epoc (ms)'][ind-1]} : {df_knie['epoc (ms)'][ind]}")
        print(f"{ind}")

for ind in df_kurbel.index:
    if ind != 0 and df_kurbel['epoc (ms)'][ind] < df_kurbel['epoc (ms)'][ind-1]:
        print(f"Kurbel: Fehler in der Matrix. Zeitfehler der Epochen")
        print(f"{df_kurbel['epoc (ms)'][ind-1]} : {df_kurbel['epoc (ms)'][ind]}")
        print(f"{ind}")
        
df_merged = pd.merge_asof(df_knie, df_kurbel, on='epoc (ms)')

# > 359 und index + 10 > 330 && < 359

for ind in df_kurbel.index:
    # wenn start nicht gefunden wird +30 stellen. dann war die beschleunigung am anfang weniger
    if df_kurbel['yaw (deg)'][ind] > 359 and 358 > df_kurbel['yaw (deg)'][ind + 10] > 330:
        start = df_kurbel['epoc (ms)'][ind]
        break

filter = ((df_merged['epoc (ms)'] >= start)&(df_merged['epoc (ms)'] <= (start + 30000)))
df_merged = df_merged[filter]
df_merged['class']=sys.argv[4]
df_merged.to_csv(sys.argv[3], index=False)